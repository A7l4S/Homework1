import java.util.Arrays;

public class SeqMergesort{

    private int[] array;
    private int[] temp;


    public SeqMergesort(int[] array){
        this.array = array;
        this.temp = new int[array.length]; 
        mergeSort(0, array.length-1);
    }

    public void mergeSort(int lo, int hi){
        if(lo < hi){
            int m = lo + (hi - lo) / 2;
            mergeSort(lo, m);
            mergeSort(m+1, hi);
            merge(lo, m, hi);
        }
    }

    public void merge(int lo, int m, int hi){
        temp = Arrays.copyOf(array, array.length);
        int i = lo;
        int j = m+1;
        int k = lo;
        while(i <= m && j <= hi){
            if(temp[i] <= temp[j]){
                array[k] = temp[i];
                i++;
            }
            else{
                array[k] = temp[j];
                j++;
            }
            k++;
        }
        while(i <= m){
            array[k] = temp[i];
            k++;
            i++;
        }
    }
}